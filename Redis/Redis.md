# Redis

## NOSQL数据库

reids是一款高性能的NOSQL系列的非关系型数据库（是单线程+多路IO复用）

**缓存：**

​	其目的是解决低速IO和高速缓存（CPU高速缓存）之间的差异

​	使用缓存的原则：二八原则：20%的热点数据以及不改变的数据放入缓存，80%的数据放入MySQL中

​			

### 非关系型数据库的优势

​	1.性能NOSQL是基于键值对的，可以想象成表中的主键和值的对应关系，而且不需要经过SQL的解析，所以性能非常高

​	2.可扩展性同样也是因为基于键值对，数据之间没有耦合性，是唯一的，所以非常容易水平扩展

### 关系型数据库的优势

​	1.复杂查询可以用SQK语句方便的在一个表以及多个表之间做非常复杂的数据查询

​	2.事务支持使得对于安全性能很高的数据访问要求得以实现。对于这两类数据库，对方的优势就是自己的弱势，反之亦然

### 总结

	关系型数据库与NoSQL数据库并非对立而是互补的关系，即通常情况下使用关系型数据库，在适合使用NoSQL的时候使用NoSQL数据库，
让NoSQL数据库对关系型数据库的不足进行弥补。
一般会将数据存储在关系型数据库中，在nosql数据库中备份存储关系型数据库的数据

## Redis概述

### 什么是Redis

redis是一个开源的key-value存储系统 ， 其大小是根据内存的大小决定的

支持存储的value数据类型有：**String**（字符串）

​													**list**（链表）

​													**set**（集合）

​													**zset**（sorted set --有序集合）

​													**hash**（哈希类型）

其操作是原子性的

支持各种不同方式的排序

数据是缓存在内存中的

会周期性的把更新的数据写入磁盘

实现了主从同步

### Redis的应用场景

• 缓存（数据查询、短连接、新闻内容、商品内容等等）
• 聊天室的在线好友列表
• 任务队列。（秒杀、抢购、12306等等）
• 应用排行榜
• 网站访问统计
• 数据过期处理（可以精确到毫秒）
• 分布式集群架构中的session分离

### Redis特性

| 速度快           | 持久化       | 多种数据结构 |
| ---------------- | ------------ | ------------ |
| 支持多种编程语言 | 功能丰富     | 源码简单     |
| 主从复制         | 高可用分布式 |              |



## Redis命令操作

### Redis键（key）

查看当前库所有的key：key *

判断某个key是否存在：exists key

查看你的key是什么类型：type key

删除指定的key数据：del key

根据value选择非阻塞删除，仅将keys从keyspace元数据中删除，真正的删除将会在后续异步删除：unlink key

为给定的key设置过期时间。10代表是10秒钟：expire key 10

查看还有多少秒过期（-1表示永不过期，-2表示已经过期）：ttl key 

切换数据库：select  (0-15)

清空当前:flushdb 

通杀全部库：flushall

### 字符串类型（String）

#### 添加操作

存储单个值（添加键值对，若存在则修改）：	    set	key	value

追加数据（给定的value将追加到原值的末尾）：  append	key	value

同时设置一个或多键值（具有修改操作）：   	    mset	key	value	key	value	key	value

设置多个键值（若设置的已存在则设置失败）：   msetnx 	key	value	key	value	key	value

#### 获取操作

获取对应的key值：                       get 	key	value

同时获取一个或多个value：       megt 	key1	 key2 	key3 

获取数据字符的个数：                 strlen 	key 

获取值的范围（起始从0开始）：getrange 	key	 起始位置 	结束位置

#### 修改操作

覆盖存储的字符串值（起始从0开始）：setrange 	key 	起始位置  	value

设置过期时间（单位是秒）：				  setex 	key	 过期时间 	value

#### 删除操作

删除key：del <key>

#### 扩展操作

数字自增:（只能对数字值操作）               incr 	key

自定义步长增加数值【整数】：                incrby 	key 	步长

自定义步长增加数值【浮点数】：            incrbyfloat 	key	 步长

设置数值数据减少指定范围的值：            decr	key	

自定义设置数值数据减少指定范围的值：decrby	key	步长

### 列表类型 list

#### 添加元素

将元素加入列表左边：lpush  key  value

将元素加入列表右边：rpush  key  value

在指定位置添加元素：linsert  key  before   value   newvalue

#### 获取元素

范围获取 ：                  lrange  key   start   stop 

查看指定索引处的值：lindex  key  index 

查看列表长度：            llen  key 

#### 删除元素

从左边删除一个值：    lpop  key 

从右边删除一个值：    rpop  key 

删除并插入列表左边：rpoplpush   key1  key2 从 key1 

删除N个指定的元素： lrem  key  n  value 

#### 修改元素

语法：lset  key  index  value 

### 集合类型 set

#### 添加元素

存储元素：sadd  key  value1  value2 

#### 获取元素

获取所有的元素：smembers   key 

获取元素个数：scard   key 

随机获取（不会从集合中删除）：srandmember   key    n 

随机并**移出**数据：spop   key 

获取交集：sinter   key1   key2 

获取并集：sunion   key1    key2 

获取差集：sdiff   key1    key2 

#### 删除元素

语法： srem   key   value1   value2 

#### set 判断和移动

判断是否包含该value：sismember   key   value 

将当前集合内容移动到另一个集合中：smove   source   destination   value

### 哈希类型 hash

#### 存储元素

存储单个值：hset   key    field    value  

批量添加或修改hash的值：hmset   key1   field1   value1   field2   value2 

当且仅当域 field 不存在 添加元素：hsetnx   key   field    value 

#### 获取元素

获取指定的field对应的值：hget   key1    field 

获取所有的field和value：hgetall   key 

获取多个filed：hmget   key    field1   field2 

获取哈希表中字段的数量：hlen   key 

获取哈希表中所有的字段名：hkeys    key 

获取所有的value：hvals   key 

#### 删除元素

语法：hdel  key  field

#### 判断和自增

判断是否存在指定的字段：hexists   key1   field 

设置指定字段的数值数据增加指定范围的值：整形：    hincrby   key   field

​																		 	浮点型：hincrbyfloat   key   field



### 有序集合Zset (sortedset)

#### 添加元素

语法：zadd    key   score1   value1   score2   value2 

#### 获取元素

按照升序获取所有元素（默认升序）：zrange   key   start   stop    WITHSCORES 

按照降序获取所有元素（默认降序）：zrevrange    key   start   stop    withscores 

查询区间的元素 【值递增(==从小到大==)次序排列】：zrangebyscore   key   min   max    withscores 

查询区间的元素 【值递增(==从大到小==)次序排列】：zrevrangebyscore   key   maxmin    withscores 

#### 删除元素

语法：zrem  key   value

#### 统计和排名

统计区间内元素的个数：zcount   key   min   max 

获取排名：					   zrank   key   value 



## Redis 发布和订阅

### 什么是发布和订阅

Redis的发布订阅（pub/sub）是一种消息通信模式：发送者（pub）发送消息，订阅者（sub）接收消息。

Redis客户端可以订阅任意数量的频道。



###  发布订阅命令行实现

订阅chan1：SUBSCRIBE chan1

通过频道chan1发布内容：publish channel1 hello



## Redis事务锁机制

### Redis的事务定义

Redis事务是一个单独的隔离操作：事务中的所有命令都会序列化、按顺序的进行。食物在执行的过程中，不会被其他的客户端罚款送来的命令请求所打断。其主要作用就是**串联多个命令防止别的命令插队。**

### Multi、Exec、discard

![image-20230926212106837](Redis.assets/image-20230926212106837.png)

从输入`Multi`命令开始，输入的命令都会依次进入命令队列中，但不会执行，直到输入`Exec`后，Redis会将之前的命令队列中的命令依次执行。

组队的过程中可以通过`discard`来放弃组队。  

`127.0.0.1:6379> multi`
`OK`
`127.0.0.1:6379(TX)> set myname zhangsan`
`QUEUED`
`127.0.0.1:6379(TX)> set mage 13`
`QUEUED`
`127.0.0.1:6379(TX)> exec`

### 事务的错误处理

组队中(`multi`)某个命令出现了报告错误，执行时整个的所有队列都会被取消。

![image-20230926212114880](Redis.assets/image-20230926212114880.png)

`127.0.0.1:6379> multi`
`OK`
`127.0.0.1:6379(TX)> set myname1 lisi`
`QUEUED`
`127.0.0.1:6379(TX)> set myname2` 
`(error) ERR wrong number of arguments for 'set' command`
`127.0.0.1:6379(TX)> set myname3 wangwu`
`QUEUED`
`127.0.0.1:6379(TX)> exec`
`(error) EXECABORT Transaction discarded because of previous errors.`

如果执行阶段(`exec`)某个命令报出了错误，则只有报错的命令不会被执行，而其他的命令都会执行，不会回滚

![image-20230926212134966](Redis.assets/image-20230926212134966.png)

`127.0.0.1:6379> multi`
`OK`
`127.0.0.1:6379(TX)> set myname1 lisi`
`QUEUED`
`127.0.0.1:6379(TX)> incr myname1`
`QUEUED`
`127.0.0.1:6379(TX)> set myname2 wangwu`
`QUEUED`
`127.0.0.1:6379(TX)> exec`

1) `OK`
2) `(error) ERR value is not an integer or out of range`
3) `OK`

### 事务冲突的问题

#### 悲观锁

悲观锁(Pessimistic Lock)==, 顾名思义，就是很悲观，每次去拿数据的时候都认为别人会修改，所以每次在拿数据的时候都会上锁，这样别人想拿这个数据就会block直到它拿到锁。==传统的关系型数据库里边就用到了很多这种锁机制==，比如`行锁`，`表锁`等，`读锁`，`写锁等`，都是在做操作之前先上**锁**。



#### 乐观锁

乐观锁(Optimistic Lock)==, 顾名思义，就是很乐观，每次去拿数据的时候都认为别人不会修改，所以不会上锁，但是在更新的时候会判断一下在此期间别人有没有去更新这个数据，可以使用版本号等机制。==乐观锁适用于多读的应用类型，这样可以提高吞吐量。==`Redis就是利用这种check-and-set机制实现事务的`。



#### WATCH key [key ...]

在执行`multi`之前，先执行`watch key1 [key2],`可以监视一个(或多个) key ，如果在事务==执行之前这个(或这些) key 被其他命令所改动，那么事务将被打断。

![image-20230926212357848](Redis.assets/image-20230926212357848.png)

#### unwatch

取消 WATCH 命令对所有 key 的监视。
如果在执行 WATCH 命令之后，EXEC 命令或DISCARD 命令先被执行了的话，那么就不需要再执行UNWATCH 了。

### Redis事务三特性

**单独的隔离操作：**事务中的所有命令都会序列化、按顺序的执行。事务在执行的过程中，不会被其他的客户端发送来的命令请求所打断。

**没有隔离级别的概念：**队列中的命令没有提交之前都不会实际的被执行，因为事务提交之前任何指令都不会被实际执行

**不保证原子性：**事务中如果有一条命令执行失败，其后的命令仍然会被执行，没有回滚



## Redis事务三特性

### 什么是redis持久化

redis是一个内存数据库，当redis服务器重启，获取电脑重启，数据会丢失，我们可以将redis内存中的数据持久化保存到硬盘的文件中

Redis提供了2个不同形式的持久化方式：RDB（Redis DataBase）、Redis DataBase

### Redis持久化之RDB

在指定的`时间间隔`内将内存中的数据集`快照`写入磁盘， 也就是行话讲的Snapshot快照，它恢复时是将快照文件直接读到内存里

### RDB是如何备份的

Redis会单独创建（fork）一个子进程来进行持久化，会`先`将数据写入到 一个`临时文件中`，待持久化过程都结束了，再用这个`临时文件替换上次持久化`好的文件。 整个过程中，主进程是不进行任何IO操作的，这就确保了极高的性能 如果需要进行大规模数据的恢复，且对于数据恢复的完整性不是非常敏感，那RDB方式要比AOF方式更加的高效。==RDB的缺点是最后一次持久化后的数据可能丢失

### Fork

Fork的作用是复制一个与当前进程`一样的进程`。新进程的所有数据（变量、环境变量、程序计数器等） 数值都和原进程一致，但是是一个全新的进程，并作为`原进程的子进程`

在Linux程序中，fork()会产生一个和父进程完全相同的子进程，但子进程在此后会被exec系统调用，出于效率考虑，Linux中引入了==“写时复制技术”==

**一般情况父进程和子进程会共用同一段物理内存**，只有进程空间的各段的内容要发生变化时，才会将父进程的内容复制一份给子进程。

### RDB持久化流程

![image-20230926212913381](Redis.assets/image-20230926212913381.png)、

### dump.rdb配置位置

在redis.conf中配置文件名称，默认为**dump.rdb**，rdb文件的保存路径，也可以修改。**默认为Redis启动时命令行所在的目录下**

![image-20230926212926896](Redis.assets/image-20230926212926896.png)

**dump.rdb的保存路径，可以根据自己的需求，修改保存的路径。**



### RDB保存（时机）策略

#### 配置文件中默认的快照配置

![image-20230926213002006](Redis.assets/image-20230926213002006.png)

**save 3600 1 :  3600秒后[1个小时]，并且有>=1个key发生变化了，才会触发保存操作。**

**save 300 100 : 5分钟后并且有>=100个key发生变化，才会触发保存操作。**

**save 60 10000: 1分钟后并且有>=10000个key发生变化，才会触发保存操作。**

#### 命令save VS bgsave

save ：save时只管保存，其它不管，全部阻塞。手动保存。不建议。

​		格式：save 秒钟 写操作次数

​		RDB是整个内存的压缩过的Snapshot，RDB的数据结构，可以配置复合的快照触发条件，
​		==默认是1分钟至少1万个key发生变化，或5分钟至少100个key发生变化，或1个小时至少1个key发生变化。==
​		**禁用：save** 

bgsave：Redis会在后台异步进行快照操作， 快照同时还可以响应客户端请求。
可以通过lastsave 命令获取最后一次成功执行快照的时间

#### flushall命令

执行flushall命令，也会产生dump.rdb文件，但里面是空的，无意义。

shutdown关闭redis时，也会持久化数据。

####  stop-writes-on-bgsave-error

![image-20230926213050199](Redis.assets/image-20230926213050199.png)

当Redis无法写入磁盘的话，直接关掉Redis的写操作。推荐yes.



#### rdbcompression 压缩文件

对于存储到磁盘中的快照，可以设置是否进行压缩存储。如果yes，redis会采用**LZF算法进行压缩**。
如果你不想消耗CPU来进行压缩的话，可以设置为关闭此功能。**推荐yes.**



#### rdbchecksum 检查完整性

![image-20230926213115768](Redis.assets/image-20230926213115768.png)

在存储快照后，还可以让redis使用CRC64算法来进行数据校验，
但是这样做会增加大约10%的性能消耗，如果希望获取到最大的性能提升，可以关闭此功能
**推荐yes.**

#### rdb的备份与恢复

先通过config get dir  查询rdb文件的目录 
将*.rdb的文件拷贝到别的地方

**rdb的恢复**

关闭Redis

* 关闭Redis
* 先把备份的文件拷贝到工作目录下 cp dump2.rdb dump.rdb
* 启动Redis, 备份数据会直接加载

### 11.2.6 RDB优势与劣势

#### RDB优势

* 适合大规模的数据恢复
* 对数据完整性和一致性要求不高更适合使用
* 节省磁盘空间
* 恢复速度快

#### RDB劣势

* Fork的时候，内存中的数据被克隆了一份，大致2倍的膨胀性需要考虑
* 虽然Redis在fork时使用了**写时拷贝技术**,但是如果数据庞大时还是比较消耗性能。
* 在备份周期在一定间隔时间做一次备份，所以如果Redis意外down掉的话，就会丢失最后一次快照后的所有修改。

### Redis持久化之AOF

#### 什么是AOF(Append Only File)

**以日志的形式来记录每个写操作（增量保存）**，将Redis执行过的所有写指令记录下来(==读操作不记录==)， **只许追加文件但不可以改写文件**，redis启动之初会读取该文件重新构建数据，换言之，redis 重启的话就根据日志文件的内容将写指令从前到后执行一次以完成数据的恢复工作

####  AOF持久化流程

（1）客户端的请求写命令会被append追加到AOF缓冲区内；

（2）AOF缓冲区根据AOF持久化策略[always,everysec,no]将操作sync同步到磁盘的AOF文件中；

（3）AOF文件大小超过重写策略或手动重写时，会对AOF文件rewrite重写，压缩AOF文件容量；

（4）Redis服务重启时，会重新load加载AOF文件中的写操作，达到数据恢复的目的；

#### AOF默认不开启

可以在redis.conf中配置文件名称，默认为 **appendonly.aof**
AOF文件的保存路径，**同RDB的路径一致**。

#### AOF和RDB同时开启

AOF和RDB同时开启，系统默认取AOF的数据（数据不会存在丢失）

#### AOF启动/修复/恢复

* AOF的备份机制和性能虽然和RDB不同, 但是备份和恢复的操作同RDB一样，都是拷贝备份文件，需要恢复时再拷贝到Redis工作目录下，启动系统即加载。
* 正常恢复
  * 修改默认的appendonly no，改为yes
  * 将有数据的aof文件复制一份保存到对应目录(查看目录：config get dir)
  * 恢复：重启redis然后重新加载

* 异常恢复
  * 修改默认的appendonly no，改为yes
  * 如遇到AOF文件损坏，通过/usr/local/bin/redis-check-aof --fix appendonly.aof进行恢复
  * 备份被写坏的AOF文件
  * 恢复：重启redis，然后重新加载

#### AOF同步频率设置

`appendfsync always`

==始终同步，每次Redis====的写入都会立刻记入日志==；性能较差但数据完整性比较好

`appendfsync everysec`

==每秒同步，每秒记入日志一次，如果宕机，本秒的数据可能丢失。==

`appendfsync no`

redis不主动进行同步，==把同步时机交给操作系统==。

#### AOF优势与劣势

##### AOF优势

 备份机制更稳健，丢失数据概率更低。

 可读的日志文本，通过操作AOF文件，可以处理误操作。

##### 劣势

* 比起RDB占用更多的磁盘空间。
* 恢复备份速度要慢。
* 每次读写都同步的话，有一定的性能压力。
* 存在个别Bug，造成恢复不能。

### RDB与AOF对比

![image-20230926213559346](Redis.assets/image-20230926213559346.png)

### RDB和AOF选择

* ==对数据敏感，建议合适AOF持久化方案==
  * AOF持久化策略使用everysecond ， 每秒钟fsync一次，该策略redis仍可以保持很好的处理性能，当出现问题时，最多会丢失0-1秒内的数据
  * AOF文件存储比较大，且恢复比较慢
* ==数据呈现阶段有效性，建议使用RDB持久化方案==
  * 数据可以良好的做到阶段内无丢失(该阶段是开发者或运维人员手工维护的)，且恢复速度比较快、阶段点数据恢复通常采用RDB方案
  * 利用RDB实现紧凑的数据持久化会使Redis降的很低
* ==综合比对==
  * RDB与AOF的选择实际上是在做一种权衡，每种都有利弊
  * 如不能承受数分钟以内的数据丢失，对业务数据非常敏感，选用AOF
  * 如能承受数分钟内的数据丢失，且追求大数据集的恢复速度，选用RDB
  * 灾难恢复选用RDB
  * 双保险策略，同时开启RDB和AOF，重启后，Redis优先使用AOF来恢复数据，降低丢失数据的量

## Redis主从复制

### 什么是主从复制

主机数据更新后根据配置和策略， 自动同步到备机的master/slaver机制，**Master以写为主，Slave以读为主**

**主从复制作用**

* 读写分离，性能扩展

* 容灾快速恢复



### 搭建主从复制

> 以一主两从为例进行演示

**拷贝多个redis.conf文件include(写绝对路径)**

开启daemonize yes

**Pid文件名字pidfile**

**指定端口port**

Log文件名字

**dump.rdb名字dbfilename**

Appendonly 关掉或者换名字

#### 创建三个配置文件

**新建redis6379.conf，填写以下内容**

`include /root/myredis/redis.conf`
`pidfile /var/run/redis_6379.pid`
`port 6379`
`dbfilename dump6379.rdb`

![image-20230926213737699](Redis.assets/image-20230926213737699.png)

**新建redis6380.conf，填写以下内容****

`include /usr/local/bin/redis.conf`
`pidfile /var/run/redis_6380.pid`
`port 6380`
`dbfilename dump6380.rdb`

![image-20230926213755976](Redis.assets/image-20230926213755976.png)

**新建redis6381.conf，填写以下内容**

`include /usr/local/bin/redis.conf`
`pidfile /var/run/redis_6381.pid`
`port 6381`
`dbfilename dump6381.rdb`

![image-20230926213810625](Redis.assets/image-20230926213810625.png)

#### 启动三台redis服务器

[root@192 bin]# redis-server ./redis6379.conf
[root@192 bin]# redis-server ./redis6380.conf
[root@192 bin]# redis-server ./redis6381.conf

​	**查看redis进程**

![image-20230926213851565](Redis.assets/image-20230926213851565.png)

#### 查看三台主机运行情况

命令:`info replication`

打印主从复制的相关信息

![image-20230926213910681](Redis.assets/image-20230926213910681.png)

#### 配从(库)不配主(库)

slaveof  <主服务器ip> <主服务器port>

成为某个实例的从服务器

**![image-20230926213933441](Redis.assets/image-20230926213933441.png)**

![image-20230926213938276](Redis.assets/image-20230926213938276.png)

**在主机上写，在从机上可以读取数据**



![image-20230926213951458](Redis.assets/image-20230926213951458.png)

![image-20230926214005049](Redis.assets/image-20230926214005049.png)

![image-20230926214009382](Redis.assets/image-20230926214009382.png)

**如果主机挂掉，重启就行，一切如初**

**如果从机挂掉了，需要重设：slaveof 主服务器ip 6379**

### 复制原理

* Slave启动成功连接到master后会发送一个**sync**命令

* Master接到命令启动后台的存盘进程，同时收集所有接收到的用于修改数据集命令，在后台进程执行完毕之后，master将传送整个数据文件到slave，以完成一次完全同步

* **全量复制**：而slave服务在接收到数据库文件数据后，将其存盘并加载到内存中。

* **增量复制**：Master继续将新的所有收集到的修改命令依次传给slave，完成同步

* 但是只要是重新连接master，一次完全同步（全量复制)将被自动执行

### Redis主从配置常用3招

#### 一主二仆

切入点问题？slave1、slave2是从头开始复制还是从切入点开始复制?比如从k4进来，那之前的k1,k2,k3是否也可以复制？

从机是否可以写？set可否？ 

主机shutdown后情况如何？从机是上位还是原地待命？

主机又回来了后，主机新增记录，从机还能否顺利复制？ 

其中一台从机down后情况如何？依照原有它能跟上大部队吗？

#### 薪火相传

上一个Slave可以是下一个slave的Master，Slave同样可以接收其他 slaves的连接和同步请求，那么该slave作为了链条中下一个的master, 可以有效减轻master的写压力,去中心化降低风险。

用 slaveof  <ip><port>

中途变更转向:会清除之前的数据，重新建立拷贝最新的

风险是一旦某个slave宕机，后面的slave都没法备份

主机挂了，从机还是从机，无法写数据了



* ##### 示例

  > 让6381成为从服务器，6380是6381的主服务器，6380的主服务器是6379。我们只需要在6381服务器中，执行 slaveof  <ip><port>命令即可。

![image-20230926214110694](Redis.assets/image-20230926214110694.png)

**此时6379下只有一台6380从服务器**

![image-20230926214148061](Redis.assets/image-20230926214148061.png)

#### 反客为主

当一个master宕机后，后面的slave可以立刻升为master，其后面的slave不用做任何修改。

用 `slaveof  no one`  将从机变为主机。

##### 示例

> 现在6379主服务器挂了，让6380做为主服务器，我们只需要在6380服务器执行slaveof  no one命令即可。

![image-20230926214201090](Redis.assets/image-20230926214201090.png)



![image-20230926214206571](Redis.assets/image-20230926214206571.png)



**反客为主的缺点是，目前只能是手动操作，不能自动切换。**

### Redis哨兵模式

#### 什么是哨兵模式

**反客为主的自动版**，能够后台监控主机是否故障，如果故障了根据投票数自动将从库转换为主库

![image-20230926214226236](Redis.assets/image-20230926214226236.png)

#### Redis哨兵模式使用步骤

##### 1、一主二仆模式，6379带着6380、6381

![image-20230926214239407](Redis.assets/image-20230926214239407.png)

##### 2、新建sentinel.conf文件

> 在redis的安装目录下新建sentinel.conf文件。我的目录是 /usr/local/bin。
>
> **注意文件的名称sentinel.conf绝不能错。**

在文件中输入以下内容:

```nginx
sentinel monitor mymaster 192.168.6.138 6379 1
```

==其中mymaster为监控对象起的服务器名称， 1 为至少有多少个哨兵同意迁移的数量。==

![image-20230926214254309](Redis.assets/image-20230926214254309.png)

##### 3、启动哨兵

命令: redis-sentinel  ./sentinel.conf

![image-20230926214302632](Redis.assets/image-20230926214302632.png)

##### 4、当主机挂掉，从机选举中产生新的主机

> 现在主服务器6379已经挂掉了，要从6380和6381两台服务器中选一个做为主服务器

![image-20230926214315156](Redis.assets/image-20230926214315156.png)

**大概10秒左右可以看到哨兵窗口日志，切换了新的主机**

![image-20230926214340200](Redis.assets/image-20230926214340200.png)

**查看6381是否是主服务器**

![image-20230926214349105](Redis.assets/image-20230926214349105.png)

同时6380的主服务器也发生了改变



**哪个从机会被选举为主机呢？根据优先级别：replica-priority** 

**原主机重启后会变为从机。**



##### 5、复制延时【缺点】

由于所有的写操作都是先在Master上操作，然后同步更新到Slave上，所以从Master同步到Slave机器有一定的延迟，当系统很繁忙的时候，延迟问题会更加严重，Slave机器数量的增加也会使这个问题更加严重。

### 故障恢复

![image-20230926214446337](Redis.assets/image-20230926214446337.png)

① 优先级在redis.conf中默认：replica-priority 100，值越小优先级越高

② 偏移量是指获得原主机数据最全的

③ 每个redis实例启动后都会随机生成一个40位的runid

![image-20230926214452829](Redis.assets/image-20230926214452829.png)
