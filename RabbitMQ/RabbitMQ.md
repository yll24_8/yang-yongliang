# RabbitMQ

# 消息中间件概述

## 为什么学习消息队列

队列主要作用就是**消除高并发访问高峰，加快网站的响应速度**

在不使用消息队列的情况下，用户的请求数据直接写入数据库，在高并发的情况下，会对数据库造成巨大的压力，同时也使得系统响应延迟加剧。

## 什么是消息中间件

也叫消息队列

MQ全称为Message Queue，消息队列是一种应用程序对应用程序的通信方法。

**消息传递：**指的是程序之间通过消息发送数据进行通信，而不是通过直接调用彼此来通信，直接调用通常是用于诸如远程过程调用的技术。

**队列：**指的是应用程序通过队列来通信。

在项目中，可将一些无无需及时返回且耗时的操作提取出来，进行**异步处理**，而这种异步处理的方式大大节省了服务器的请求响应时间，从而**提高了系统的吞吐量**。

## 应用场景

消息中间件的重要作用就是：

​	**异步处理**

​	**解耦服务**

​	**流量削峰**

### 应用解耦

​	传统模式的缺点：系统间的耦合性太高，每次都要修改，过于麻烦。

​	中间件模式的优点：将消息写入消息队列，需要消息的系统自己从消息队列中订阅，从而系统接收时不需要做任何修改。

### 异步处理

​	传统有串行和并行的方式：一些非不要的业务逻辑以**同步**的方式运行，太耗费时间

​	消息队列的方式：非必要的业务逻辑以**异步**的方式运行，加快响应速度

### 流量削峰

​	一般在**秒杀**的活动中应用广泛

​	场景：秒杀活动，一般会因为流量过大，导致应用挂掉，为了解决这个问题，一般在应用前端加入消息队列。

​	**传统模式的缺点**：并发量大的时候，所有的请求直接怼到数据库，造成数据库连接异常。

​	**中间件的模式的优点**：系统A慢慢的按照数据库能处理的并发量，从消息队列中慢慢拉取消息。在生产中这个短暂的高峰期挤压是允许的

​	也叫做削峰填谷：使用了MQ之后，限制消费消息的速度为1000，但是这样一来，高峰产生的数据势必会积压在MQ中，高峰就被“削”掉了。但是因为消息挤压，在高峰之后的一段时间，消费消息的速度还是会维持在3消费完挤压的消息，这就是“填谷”

### 什么是QPS、PV、UV、PR

**QPS**：即每秒查询率，是对一个特定的查询服务器在**规定时间内所处理流量多少**的衡量标准。

​	**每秒查询率**

​	因特网上，经常使用**每秒查询率**来衡量域名系统服务器的机器的性能，即为QPS

​	或者理解为：**每秒的响应请求数**，也就是**最大吞吐能力**



**PV：**就是页面浏览量，或点击量；通常是衡量一个网络新闻或者网站甚至一条网络新闻的主要指标。

​		PV之于网站，就像收视率之于电视，是投资者衡量商业网站表现的最重要尺度。



**UV：**指的是，访问某个站点或点击某条新闻的不同IP地址的人数。



**PR**：网页的级别技术，**用来标识网页的等级**/重要性。1级到10级，10级为满分。

## AMQP、JMS

MQ是消息通信的模型；实现MQ的大致有梁总主流方式：AMQP、JMS

### AMQP

是一种协议，准确的说就是一种链接协议。这是和JMS的本质差别，其不从API层进行限定，而是直接定义网络交换的数据格式。

AMQP  一个提供统一消息服务的应用层标准**高级消息队列协议**，是应用层协议的一个开发标准，为面向消息的中间件设计

RabbitMQ时AMQP协议的Erlang语言实现

| 概念           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| 连接Connection | 一个网络连接，比如TCP/IP套接字连接。                         |
| 信道Channel    | 多路复用连接中的一条独立的双向数据流通道。为会话提供物理传输介质。 |
| 客户端Client   | AMQP连接或者会话的发起者。AMQP是非对称的，客户端生产和消费消息，服务器存储和路由这些消息。 |
| 服务节点Broker | 消息中间件的服务节点；一般情况下可以将一个RabbitMQ Broker看作一台RabbitMQ 服务器。 |
| 端点           | AMQP对话的任意一方。一个AMQP连接包括两个端点（一个是客户端，一个是服务器）。 |
| 消费者Consumer | 一个从消息队列里请求消息的客户端程序。                       |
| 生产者Producer | 一个向交换机发布消息的客户端应用程序。                       |

### JMS

JMS即Java消息服务（JavaMessage Service）应用程序接口，是一个Java平台中和关于面向消息中间件的API，用于在两个应用程序之间，或分布式系统中发送消息，进行异步通信。

###  AMQP 与 JMS 区别

- JMS是定义了统一的接口，来对消息进行统一；	AMQP是通过规定协议来统一数据交互的格式
- JMS限定了必须使用Java语言；                                AMQP知识协议，不规定实现方式，因此是跨语言的
- JMS规定了两种消息模式；                                       AMQP的消息模式更加丰富

# RabbMQ概述

由erlang语言进行开发，基于AMQP协议实现的消息队列，是一种应用程序之间的通信方法，消息队列在分布式系统开发中应用非常广泛

RabbMQ提供了6种模式：简单模式、work模式、发布与订阅模式Publish/Subscribe、路由模式Routing、主题模式Topics、PRC远程调用模式（不太算MQ，不介绍）

![image-20231009171548054](RabbitMQ.assets/image-20231009171548054.png)

## RabbMQ简介

AMQP（ Advanced Message Queuing Protocol）高级消息队列，是一个网络协议，是应用层协议的一个开发标准，为面向消息的中间件设计，基于此协议的客户端与消息中间件可传递消息，并不受客户端/中间件不同产品、不同开发语言等条件的限制。

RabbitMQ 基础架构如下图：

![image-20231009171536506](RabbitMQ.assets/image-20231009171536506.png)

## RabbitMQ 中的相关概念

**Broker**：接收和分发消息的应用，RabbitMQ Server就是 Message Broker

**Virtual host**：出于多租户和安全因素设计的，把 AMQP 的基本组件划分到一个虚拟的分组中，类似于网络中的 namespace 概念。当多个不同的用户使用同一个 RabbitMQ server 提供的服务时，可以划分出多个vhost，每个用户在自己的 vhost 创建 exchange／queue 等
**Connection**： publisher／consumer 和 broker 之间的 TCP 连接

**Channel**：如果每一次访问 RabbitMQ 都建立一个 Connection，在消息量大的时候建立 TCP Connection的开销将是巨大的，效率也较低。Channel 是在 connection 内部建立的逻辑连接，如果应用程序支持多线程，通常每个thread创建单独的 channel 进行通讯，AMQP method 包含了channel id 帮助客户端和message broker 识别 channel，所以 channel 之间是完全隔离的。Channel 作为轻量级的 Connection 极大减少了操作系统建立 TCP connection 的开销

**Exchange**： message 到达 broker 的第一站，根据分发规则，匹配查询表中的 routing key，分发消息到queue 	中去。常用的类型有：direct (point-to-point), topic (publish-subscribe) and fanout (multicast)

**Queue**：消息最终被送到这里等待 consumer 取走

**Binding**： exchange 和 queue 之间的虚拟连接，binding 中可以包含 routing key。Binding 信息被保存到 exchange 中的查询表中，用于 message 的分发依据

# RabbitMQ快速入门

### 添加依赖

```xml
<dependency>
    <groupId>com.rabbitmq</groupId>
    <artifactId>amqp-client</artifactId>
    <version>5.6.0</version>
</dependency>
```

### 编写RabbitMQ工具类

```java
public class RabbitMQUtil {
    public static Connection getConnection() throws  Exception{
        // 1，创建连接工厂对象
        ConnectionFactory factory = new ConnectionFactory();
        // 1.1 设置ip地址    默认 ： localhost
        factory.setHost("localhost");
        // 1.2 设置端口号   默认： 5672
        factory.setPort(5672);
        // 1.3 设置 虚拟主机  默认： /
        factory.setVirtualHost("/");
        // 1.4 设置用户名   默认 ： guest
        factory.setUsername("guest");
        // 1.5 设置密码    默认 ： guest
        factory.setPassword("guest");
        // 2，使用连接工厂对象来创建一个连接对象
        Connection connection = factory.newConnection();
        return connection;
    }
}
```

### 编写生产者

```java
package basicproducer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import util.RabbitMQUtil;

public class BasicProducer {
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        /**
         * queue      参数1：队列名称
         * durable    参数2：是否定义持久化队列,当mq重启之后,还在
         * exclusive  参数3：是否独占本次连接
         *            ① 是否独占,只能有一个消费者监听这个队列
         *            ② 当connection关闭时,是否删除队列
         * autoDelete 参数4：是否在不使用的时候自动删除队列,当没有consumer时,自动删除
         * arguments  参数5：队列其它参数
         */
        channel.queueDeclare("hello",true,false,false,null);
        //发送消息
        String message = "zysbeautiful!!!!!!!!";
        /**
         * 参数1：交换机名称，如果没有指定则使用默认Default Exchage
         * 参数2：路由key,简单模式可以传递队列名称
         * 参数3：消息其它属性
         * 参数4：消息内容
         */
        channel.basicPublish("","hello",null,message.getBytes());
        //释放资源
        channel.close();
        connection.close();
    }
}
```

### 编写消费者

```java
package basicconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


public class BasicConsumer {
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare("hello",true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume("hello",true,defaultConsumer);

        //释放资源（一般不做）
    }
}

```

# RabbitMQ工作模式

## Work queues工作队列模式

简单来说就是多个消费端同时消息同一个队列种的消息，是竞争关系

### 生产者

```java
package workerproducer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import util.RabbitMQUtil;

public class WorkProducer {
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare("work_queue",true,false,false,null);
        for (int i = 0; i <20 ;i++) {
            //发送消息
            String message = "zysbeautiful!!!!!!!!work---------------" + i;
            channel.basicPublish("","work_queue",null,message.getBytes());
        }

        //释放资源
        channel.close();
        connection.close();
    }
}

```

### 消费者1

```java
package workconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class WorkerConsumer01 {
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare("work_queue",true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume("work_queue",true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

### 消费者2

```java
package workconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class WorkerConsumer02 {
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare("work_queue",true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume("work_queue",true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

## Publish/Subscribe发布与订阅模式

**Exchange，只负责转发消息，不具备存储信息的能力**，因此如果没有任何队列与Exchange绑定，或者没有符合路由规则的队列，那么消息就会丢失！

绑定后，消费者均会收到生产者发布的消息

### 生产者

```java
	package fanoutproducer;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import util.RabbitMQUtil;

public class FanoutProducer {
    public static final String QUEUE_NAME_ONE = "fanout_queue1";
    public static final String QUEUE_NAME_TWO = "fanout_queue2";
    public static final String EXCHANGE_NAME = "fanout_exchange";
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建队列
        Channel channel = connection.createChannel();

        //声明交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT,true,false,null);
        //创建队列
        channel.queueDeclare(QUEUE_NAME_ONE,true,false,false,null);
        channel.queueDeclare(QUEUE_NAME_TWO,true,false,false,null);
        //绑定队列
        channel.queueBind(QUEUE_NAME_ONE,EXCHANGE_NAME,"");
        channel.queueBind(QUEUE_NAME_TWO,EXCHANGE_NAME,"");

        //发送消息
        String message = "发布与订阅发送到one和two!!!!!!!!";
        channel.basicPublish(EXCHANGE_NAME,"",null,message.getBytes());

        //释放资源
        channel.close();
        connection.close();
    }
}

```

### 消费者1

```java
	package fanoutconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class FanoutConsumer01 {
    public static final String QUEUE_NAME_ONE = "fanout_queue1";
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare(QUEUE_NAME_ONE,true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume(QUEUE_NAME_ONE,true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

### 消费者2

```java
package fanoutconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class FanoutConsumer02 {
    public static final String QUEUE_NAME_TWO = "fanout_queue2";

    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //
        Channel channel = connection.createChannel();
        //绑定队列
        channel.queueDeclare(QUEUE_NAME_TWO,true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume(QUEUE_NAME_TWO,true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

## Routing路由模式

路由模式特点：

- 队列与交换机的绑定，不能是任意绑定了，而是要指定一个`RoutingKey`（路由key）
- 消息的发送方在 向 Exchange发送消息时，也必须指定消息的 `RoutingKey`。
- Exchange不再把消息交给每一个绑定的队列，而是根据消息的`Routing Key`进行判断，只有队列的`Routingkey`与消息的 `Routing key`完全一致，才会接收到消息

在编码上与 `Publish/Subscribe发布与订阅模式` 的区别是交换机的类型为：Direct，还有队列绑定交换机的时候需要指定routing key。

Routing模式要求队列在绑定交换机时要指定routing key，消息会转发到符合routing key的队列。

### 生产者

```java
package directproducer;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import util.RabbitMQUtil;

public class DirectProducer {
    public static final String QUEUE_NAME_ONE = "direct_queue1";
    public static final String QUEUE_NAME_TWO = "direct_queue2";
    public static final String EXCHANGE_NAME = "fanout_exchange";
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();

        //声明交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT,true,false,null);
        //创建队列
        channel.queueDeclare(QUEUE_NAME_ONE,true,false,false,null);
        channel.queueDeclare(QUEUE_NAME_TWO,true,false,false,null);
        //绑定队列
        channel.queueBind(QUEUE_NAME_ONE,EXCHANGE_NAME,"info");
        channel.queueBind(QUEUE_NAME_ONE,EXCHANGE_NAME,"debug");
        channel.queueBind(QUEUE_NAME_TWO,EXCHANGE_NAME,"info");
        channel.queueBind(QUEUE_NAME_TWO,EXCHANGE_NAME,"error");

        //发送消息
        String message = "定向模式!!!!!!!!";
        channel.basicPublish(EXCHANGE_NAME,"error",null,message.getBytes());

        //释放资源
        channel.close();
        connection.close();
    }
}

```

### 消费者1

```java
package directconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class DirectConsumer01 {
    public static final String QUEUE_NAME_ONE = "direct_queue1";
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare(QUEUE_NAME_ONE,true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume(QUEUE_NAME_ONE,true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

### 消费者2

```java
package directconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class DirectConsumer02 {
    public static final String QUEUE_NAME_TWO = "direct_queue2";

    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //
        Channel channel = connection.createChannel();
        //绑定队列
        channel.queueDeclare(QUEUE_NAME_TWO,true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume(QUEUE_NAME_TWO,true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

## Topics通配符模式

`Topic`类型与`Direct`相比，都是可以根据`RoutingKey`把消息路由到不同的队列。只不过`Topic`类型`Exchange`可以让队列在绑定`Routing key` 的时候**使用通配符**！

`Routingkey` 一般都是有一个或多个单词组成，多个单词之间以”.”分割，例如： `item.insert`

 通配符规则：

`#`：匹配一个或多个词

`*`：匹配不多不少恰好1个词

举例：

`item.#`：能够匹配`item.insert.abc` 或者 `item.insert`

`item.*`：只能匹配`item.insert`



Topic主题模式可以实现 `Publish/Subscribe发布与订阅模式` 和 ` Routing路由模式` 的功能；只是Topic在配置routing key 的时候可以使用通配符，显得更加灵活。

### 生产者

```java
package topicproducer;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import util.RabbitMQUtil;

public class TopicProducer {
    public static final String QUEUE_NAME_ONE = "topic_queue1";
    public static final String QUEUE_NAME_TWO = "topic_queue2";
    public static final String EXCHANGE_NAME = "fanout_exchange";
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();

        //声明交换机
        /**
         * @param exchange 交换机名称
         * @param type 交换机类型 DIRECT("direct"), FANOUT("fanout"), TOPIC("topic"), HEADERS("headers");
         * @param durable 指定交换机是否是持久的
         * @param autoDelete 指定交换机是否是自动删除的
         * @param internal 指定交换机是否是内部使用
         * @param arguments 其他参数
         */
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC,true,false,null);
        //创建队列
        channel.queueDeclare(QUEUE_NAME_ONE,true,false,false,null);
        channel.queueDeclare(QUEUE_NAME_TWO,true,false,false,null);
        //绑定队列
        channel.queueBind(QUEUE_NAME_ONE,EXCHANGE_NAME,"#.fxp.#");
        channel.queueBind(QUEUE_NAME_ONE,EXCHANGE_NAME,"itfxp.#");
        channel.queueBind(QUEUE_NAME_TWO,EXCHANGE_NAME,"www.itfxp.*");
        channel.queueBind(QUEUE_NAME_TWO,EXCHANGE_NAME,"atitfxp.*");

        //发送消息
        String message = "该条消息发送到#.fxp.#匹配的队列中";
        channel.basicPublish(EXCHANGE_NAME,"#.fxp.#",null,message.getBytes());

        //释放资源
        channel.close();
        connection.close();
    }
}

```

### 消费者1

```java
package topicconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class TopicConsumer01 {
    public static final String QUEUE_NAME_ONE = "topic_queue1";
    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare(QUEUE_NAME_ONE,true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume(QUEUE_NAME_ONE,true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

### 消费者2

```java
package topicconsumer;

import com.rabbitmq.client.*;
import util.RabbitMQUtil;

import java.io.IOException;


public class TopicConsumer02 {
    public static final String QUEUE_NAME_TWO = "topic_queue2";

    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMQUtil.getConnection();
        //
        Channel channel = connection.createChannel();
        //绑定队列
        channel.queueDeclare(QUEUE_NAME_TWO,true,false,false,null);


        //创建默认的回调函数
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            /*
            * envelope:获取路由，队列的相关信息
            * byte[] body：消费的信息内容
            * */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //获取交换机信息
                String exchange = envelope.getExchange();
                System.out.println("exchange=" + exchange);
                String routingKey = envelope.getRoutingKey();
                System.out.println("routingKey=" + routingKey);

                String message = new String(body,"utf-8");
                System.out.println("message=" + message);
            }
        };

        channel.basicConsume(QUEUE_NAME_TWO,true,defaultConsumer);


        //释放资源（一般不做）



    }
}

```

## 模式总结

RabbitMQ工作模式：
**1、简单模式 HelloWorld**
一个生产者、一个消费者，不需要设置交换机（使用默认的交换机）

**2、工作队列模式 Work Queue**
一个生产者、多个消费者（竞争关系），不需要设置交换机（使用默认的交换机）

**3、发布订阅模式 Publish/subscribe**
需要设置类型为fanout的交换机，并且交换机和队列进行绑定，当发送消息到交换机后，交换机会将消息发送到绑定的队列

**4、路由模式 Routing**
需要设置类型为direct的交换机，交换机和队列进行绑定，并且指定routing key，当发送消息到交换机后，交换机会根据routing key将消息发送到对应的队列

**5、通配符模式 Topic**
需要设置类型为topic的交换机，交换机和队列进行绑定，并且指定通配符方式的routing key，当发送消息到交换机后，交换机会根据routing key将消息发送到对应的队列

# 
